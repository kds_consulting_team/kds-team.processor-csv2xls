# CSV 2 XLSX processor

Converts all files present in the `data/in/tables/` into `xlsx` format. The result files are stored in `data/out/tables/`

**Table of contents:**  
  
[TOC]

## Functional notes

- The processor does not support sliced files
- The processor does not generate manifest

The usecase intended is exporting data from KBC Storage. For instance when exporting to FTP/S3.

## Configuration

- **export_mode** - *[OPT]* Default value: `separate_files`
    - `single_file_merge` - all input files will be merged into a single file named `kbc_export.xlsx`. 
    The files are expected to have same structure
    - `separate_files` - All input files will be exported in separate xlsx files. Each file will keep its name, 
    e.g. `data/in/tables/sample.csv` -> `data/out/tables/sample.xlsx`
    - `single_file_tabs` - All input files will be merged in one `kbc_export.xlsx`, each file on separate sheet.
- **header_override** - *[OPT]* mapping of alternate headers for input files. Useful when you need to use characters not allowed in the storage tables.
  e.g.
```"header_override": {
      "sample.csv": [
        "A",
        "Campaign Name",
        "Status",
        "Start_Date",
        "End_Date",
        "Location",
        "Eventbrite_link"
      ]
    }
  ```
- **strings_to_urls** - *[OPT]* By default, this processor refrains from formatting URL strings as hyperlinks due to the
    Excel worksheet's constraint of accommodating only up to 65,530 URLs in a single sheet. If you enable this parameter (set it to true), the processor will treat URLs as hyperlinks.

### Sample configuration

```json
{
    "definition": {
        "component": "kds-team.processor-csv2xls"
    },
    "parameters": {
    	"export_mode": "separate_files",
      "header_override": {
      "sample.csv": [
        "A",
        "Campaign Name",
        "Status",
        "Start_Date",
        "End_Date",
        "Location",
        "Eventbrite_link"
      ]
    }
	}
}
```
 
# Development
 
This example contains runnable container with simple unittest. For local testing it is useful to include `data` folder in the root
and use docker-compose commands to run the container or execute tests. 

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path:
```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone https://bitbucket.org:kds_consulting_team/kds-team.processor-csv2xlsx.git my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Testing

The preset pipeline scripts contain sections allowing pushing testing image into the ECR repository and automatic 
testing in a dedicated project. These sections are by default commented out. 

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) 