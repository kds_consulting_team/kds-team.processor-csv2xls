import csv
import glob
import logging
import ntpath
import os
from pathlib import Path
from typing import Iterable

import xlsxwriter
from keboola.component import ComponentBase, UserException


# configuration variables
DEFAULT_EXPORT_NAME = 'kbc_export.xlsx'
KEY_EXPORT_MODE = 'export_mode'
KEY_STRING_TO_URL = 'strings_to_urls'

KEY_OVERRIDE_HEADER = 'header_override'

SUPPORTED_MODES = ["single_file_merge", "separate_files", "single_file_tabs"]

MANDATORY_PARS = []
MANDATORY_IMAGE_PARS = []


class Component(ComponentBase):

    def __init__(self):
        super().__init__()

        try:
            self.validate_configuration_parameters(MANDATORY_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

    def run(self):
        '''
        Main execution code
        '''
        params = self.configuration.parameters  # noqa
        tables = [f for f in glob.glob(self.tables_in_path + "/*[!.manifest]", recursive=True)]
        export_mode = params.get(KEY_EXPORT_MODE, 'separate_files')

        options = {'constant_memory': True}

        if not params.get(KEY_STRING_TO_URL):
            options[KEY_STRING_TO_URL] = False

        if export_mode == "single_file_merge":
            logging.info(F'Converting files using {export_mode} mode')
            self.export_tables_to_single_file(tables, DEFAULT_EXPORT_NAME, options)

        elif export_mode == "single_file_tabs":
            logging.info(F'Converting files using {export_mode} mode')
            self.export_tables_to_single_file_tabs(tables, DEFAULT_EXPORT_NAME, options)

        elif export_mode == "separate_files":
            logging.info(F'Converting files using {export_mode} mode')
            self.export_tables_to_separate_files(tables, options)
        else:
            raise ValueError(F'Invalid export mode "{export_mode}"! Supported values are: {SUPPORTED_MODES}')

    def export_tables_to_separate_files(self, tables, options):
        for t in tables:
            self.export_tables_to_single_file([t], ntpath.basename(t).replace('.csv', '') + '.xlsx',
                                              options)

    def export_tables_to_single_file(self, tables, export_file_name, options):
        workbook = xlsxwriter.Workbook(os.path.join(self.tables_out_path, export_file_name),
                                       options)
        worksheet = workbook.add_worksheet()
        bold = workbook.add_format({'bold': True})
        last_row = 0
        for i, t in enumerate(tables):
            logging.info(F'Converting file "{t}" into "{export_file_name}"')
            with open(t, mode='rt', encoding='utf-8') as in_file:
                reader = csv.DictReader(in_file, lineterminator='\n', delimiter=',', quotechar='"')
                if i == 0:
                    current_header = reader.fieldnames
                    header = self._build_header(t, current_header)
                    self._write_xls_sheet_header(header, worksheet, bold)
                    last_row = 0
                last_row = self._write_xls_sheet(reader, worksheet, last_row + 1)

        workbook.close()

    def export_tables_to_single_file_tabs(self, tables, export_file_name, options):
        workbook = xlsxwriter.Workbook(os.path.join(self.tables_out_path, export_file_name),
                                       options)

        bold = workbook.add_format({'bold': True})
        for i, t in enumerate(tables):
            logging.info(F'Converting file "{t}" into "{export_file_name}"')
            worksheet = workbook.add_worksheet(name=ntpath.basename(t).replace('.csv', ''))

            with open(t, mode='rt', encoding='utf-8') as in_file:
                reader = csv.DictReader(in_file, lineterminator='\n', delimiter=',', quotechar='"')
                current_header = reader.fieldnames
                header = self._build_header(t, current_header)
                self._write_xls_sheet_header(header, worksheet, bold)
                last_row = 0
                self._write_xls_sheet(reader, worksheet, last_row + 1)
        workbook.close()

    def _build_header(self, table_path: str, header: Iterable[str]):
        table_name = Path(table_path).name
        override_header = self.configuration.parameters.get(KEY_OVERRIDE_HEADER, {})
        if override_header.get(table_name):
            header = override_header[table_name]

        return header

    # ################  XLS Writer methods
    def _write_xls_sheet_header(self, header, worksheet, header_format):
        for c, col in enumerate(header):
            worksheet.write_string(0, c, col, cell_format=header_format)

    def _write_xls_sheet(self, reader, worksheet, start_row_index):
        r = 0
        max_col_sizes = dict()
        for r, row in enumerate(reader):
            for c, col in enumerate(row):
                max_col_sizes[c] = len(row[col]) + 1 if len(row[col]) + 1 > max_col_sizes.get(c, 0) \
                    else max_col_sizes.get(c, 0)
                worksheet.write(start_row_index + r, c, row[col])
                worksheet.set_column(c, c, max_col_sizes[c])
        return start_row_index + r


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
