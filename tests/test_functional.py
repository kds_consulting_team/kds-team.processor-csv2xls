import os
import unittest
from pathlib import Path

import pandas as pd
from datadirtest import DataDirTester, TestDataDir


class CompareBinary(TestDataDir):
    def assert_directory_files_contents_match(self, files_expected_path: str, files_real_path: str):
        """
        Tests whether files in two directories are the same.
        If not the error message prints out which files differ in each directory

        Args:
            files_expected_path:  Path holding expected files
            files_real_path: Path holding real/source files
        """
        file_paths = self.get_all_files_in_dir(files_expected_path)
        common_files = [file.replace(files_expected_path, "").strip("/").strip('\\') for file in file_paths]
        mismatch = []
        for f in common_files:
            print(f'Diff of {f}, expected vs real: ')
            df1 = pd.read_excel(os.path.join(files_expected_path, f), engine='openpyxl')
            df2 = pd.read_excel(os.path.join(files_real_path, f), engine='openpyxl')
            if not df1.equals(df2):
                mismatch.append(f)
                difference = df1[df1 != df2]
                print(difference)

        self.assertEqual(mismatch, [], f" Files : {mismatch} do not match")


class TestComponent(unittest.TestCase):

    def test_functional(self):
        functional_tests = DataDirTester(Path(__file__).parent.joinpath('functional'),
                                         Path(__file__).parent.parent.joinpath('src/component.py'),
                                         test_data_dir_class=CompareBinary)
        functional_tests.run()


if __name__ == "__main__":
    unittest.main()
